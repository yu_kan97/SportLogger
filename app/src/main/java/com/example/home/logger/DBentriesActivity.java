package com.example.home.logger;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class DBentriesActivity extends AppCompatActivity implements View.OnClickListener{

    DatabaseHelper dbHelper = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dbentries);

        dbHelper = new DatabaseHelper(getApplicationContext());
        ArrayList<String> list = getIntent().getStringArrayListExtra("key");

        LinearLayout lView = (LinearLayout) findViewById(R.id.dbentries);


        for (int i=0; i<list.size();i++){

            Button showButton = new Button(this);

            showButton.setTextSize(25);
            showButton.setText(list.get(i));
            showButton.setOnClickListener(this);
            String text = list.get(i);
            text = text.replaceAll("\\D+", "");
            int e_id = Integer.parseInt(text);
            showButton.setId(e_id);

            lView.addView(showButton);

        }
    }

    @Override
    public void onClick(View v) {
        int buttonId =  v.getId();
        dbHelper.queryLocations(buttonId);

        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("RESULTS", dbHelper.getResults());
        startActivity(intent);
    }
}
