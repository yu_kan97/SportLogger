package com.example.home.logger;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.TreeSet;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String TAG = "DBHelper";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "SportLogger.db";
    public static final String TABLE_NAME = "locations";
    public static final String COLUMN_NAME_LATITUDE = "lat";
    public static final String COLUMN_NAME_LONGITUDE = "lon";
    public static final String COLUMN_NAME_DATE = "date";
    public static final String COLUMN_NAME_TIME = "time";
    public static final String COLUMN_NAME_ACCURACY = "accuracy";

    public static final String SQL_CREATE = "create table " + TABLE_NAME +
            " (_id integer primary key autoincrement, " + COLUMN_NAME_LATITUDE + " real, "
            + COLUMN_NAME_LONGITUDE + " real, " + COLUMN_NAME_DATE + " text, " + COLUMN_NAME_TIME + " text, "
            + COLUMN_NAME_ACCURACY + " real)";

    public static final String SQL_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public HashMap<Float, LatLng> results = new HashMap<>();
    String s_date;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.w(TAG, "onCreate");
        db.execSQL(SQL_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP);
        onCreate(db);

    }

    public void removeAll(){
        SQLiteDatabase db = getReadableDatabase();

        db.execSQL("DELETE from " + TABLE_NAME);
        db.close();
    }

    void addLocation(double lat, double lon, String date, String time, float accuracy) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_LATITUDE, lat);
        values.put(COLUMN_NAME_LONGITUDE, lon);
        values.put(COLUMN_NAME_DATE, date);
        values.put(COLUMN_NAME_TIME, time);
        values.put(COLUMN_NAME_ACCURACY, accuracy);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(DatabaseHelper.TABLE_NAME, null, values);
        db.close();
        Log.i(TAG, "Database insertion returned:" + id);

    }

    public TreeSet<String> getRows() {
        String [] columns = {COLUMN_NAME_DATE};
        TreeSet<String> a = new TreeSet<>();
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = db.query(TABLE_NAME, columns, null, null, null, null, null);
            Log.i(TAG, "Locations returned: " + c.getCount());
            while (c.moveToNext()) {

                String date = c.getString(c.getColumnIndexOrThrow(COLUMN_NAME_DATE));

                a.add(date);
            }
            c.close();
            db.close();
        } catch (Throwable t) {Log.e(TAG,"getRows: " + t.toString(),t);}
        return a;
    }


    public void queryLocations(int querryBy){

        int date = querryBy;

        int day = date/1000000;

        int monthAndYear = date%1000000;
        Log.d(TAG, String.valueOf(monthAndYear));

        int month = monthAndYear/10000;

        int year = monthAndYear%10000;

        String s_day = String.valueOf(day);
        String s_month;
        if (month >= 1 && month <= 9){
            s_month = "0" + String.valueOf(month);
        } else {
            s_month = String.valueOf(month);
        }
        String s_year = String.valueOf(year);

        s_date = s_day + "." + s_month + "." + s_year;

        String [] columns = {COLUMN_NAME_LATITUDE, COLUMN_NAME_LONGITUDE, COLUMN_NAME_DATE, COLUMN_NAME_ACCURACY};

        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = db.query(TABLE_NAME, columns, "date like '" + s_date + "'", null, null, null, null);
            Log.i(TAG, "Locations returned: " + c.getCount());
            while (c.moveToNext()) {

                double lat = c.getDouble(c.getColumnIndexOrThrow(COLUMN_NAME_LATITUDE));
                double lon = c.getDouble(c.getColumnIndexOrThrow(COLUMN_NAME_LONGITUDE));
                float accuracy = c.getFloat(c.getColumnIndexOrThrow(COLUMN_NAME_ACCURACY));

                results.put(accuracy, new LatLng(lat, lon));
            }
            c.close();
            db.close();
        } catch (Throwable t) {Log.e(TAG,"getRows: " + t.toString(),t);}

    }

    public HashMap<Float, LatLng> getResults() {
        Log.i(TAG,"getResult: " + results.entrySet());
        return results;
    }

}