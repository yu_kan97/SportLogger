package com.example.home.logger;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class ForegroundService extends Service implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private static final String LOG_TAG = "ForegroundService";
    private static final String TAG = "LocationActivity";
    public static boolean IS_SERVICE_RUNNING = false;
//    private static final int MY_PERMISSION_ACCESS_COARSE_LOCATION = 11;
//    private static final int MY_PERMISSION_ACCESS_FINE_LOCATION = 12;


    GoogleApiClient locClient;
    LocationRequest locRequest;
    Location lastLocation;
    private FusedLocationProviderApi fusedLocationProviderApi = LocationServices.FusedLocationApi;
    public DatabaseHelper dbHelper;

    String lastUpdateDate;
    String lastUpdateTime;

    @Override
    public void onCreate() {
        super.onCreate();

        locRequest = new LocationRequest();
        locRequest.setInterval(10000);
        locRequest.setFastestInterval(5000);
        locRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        locClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


        dbHelper = new DatabaseHelper(getApplicationContext());

    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){

        if (intent.getAction().equals("Start")) {
            Log.i(LOG_TAG, "Received Start Foreground Intent ");
            Intent notificationIntent = new Intent(this, MainActivity.class);
            notificationIntent.setAction("MAIN_ACTION");
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    notificationIntent, 0);

            Notification notification = new Notification.Builder(this)
                            .setContentTitle("Sport Logger")
                            .setContentText("Location")
                            .setContentIntent(pendingIntent)
                            .setTicker("Sport Logger")
                            .build();

            startForeground(101,
                    notification);

            if (locClient != null) {
                locClient.connect();
            }


        } else if (intent.getAction().equals("Stop")) {
            Log.i(LOG_TAG, "Received Stop Foreground Intent");
            if (locClient.isConnected()) {
                locClient.disconnect();
            }
            dbHelper.close();
            stopForeground(true);
            stopSelf();
         }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(LOG_TAG, "In onDestroy");
    }

    @Override
    public void onConnected(Bundle bundle) {
        startLocationUpdates();
    }

    public void startLocationUpdates() {

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            fusedLocationProviderApi.requestLocationUpdates(
                    locClient, locRequest, this);

        } else {


            Log.d(LOG_TAG, "No permissions");
//            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
//                ActivityCompat.requestPermissions(this,
//                        new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
//                        MY_PERMISSION_ACCESS_COARSE_LOCATION);
//            }
//
//            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
//                ActivityCompat.requestPermissions(this,
//                        new String[] { android.Manifest.permission.ACCESS_FINE_LOCATION },
//                        MY_PERMISSION_ACCESS_FINE_LOCATION);
//            }

        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Connection failed: " + connectionResult.toString());
    }

    @Override
    public void onLocationChanged(Location location) {
        lastLocation = location;

        double latitude = lastLocation.getLatitude();
        double longtitude = lastLocation.getLongitude();

        Date date = new Date(location.getTime());
        DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
        lastUpdateDate = dateFormat.format(date);

        lastUpdateDate = lastUpdateDate.replaceAll("\\D+", ".");

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
        Date currentLocalTime = cal.getTime();
        DateFormat time = android.text.format.DateFormat.getTimeFormat(getApplicationContext());
        time.setTimeZone(TimeZone.getTimeZone("GMT+1:00"));

        lastUpdateTime = time.format(currentLocalTime);

        float accuracy = lastLocation.getAccuracy();

        dbHelper.addLocation(latitude, longtitude, lastUpdateDate, lastUpdateTime, accuracy);
    }

}
