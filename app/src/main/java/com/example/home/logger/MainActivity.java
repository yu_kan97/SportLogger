package com.example.home.logger;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.TreeSet;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button start_button;

    public DatabaseHelper dbHelper;

    private static final String TAG = "LocationActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        start_button = (Button) findViewById(R.id.start_button);
        start_button.setOnClickListener(this);

        dbHelper = new DatabaseHelper(getApplicationContext());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.log_show:
                Intent intent = new Intent(this, DBentriesActivity.class);
                intent.putStringArrayListExtra("key", getEntries());
                startActivity(intent);
                return true;
            case R.id.log_delete:
                dbHelper.removeAll();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {

        Intent service = new Intent(MainActivity.this, ForegroundService.class);
        if (start_button.getText().equals("Start")) {
            service.setAction("Start");
            ForegroundService.IS_SERVICE_RUNNING = true;
            start_button.setText("Stop");
        } else {
            service.setAction("Stop");
            ForegroundService.IS_SERVICE_RUNNING = false;
            start_button.setText("Start");

        }
        startService(service);

    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onStop() {
        super.onStop();
    }


        public ArrayList<String> getEntries(){

        TreeSet<String> treeSet = dbHelper.getRows();
        ArrayList<String> entries = new ArrayList<>(treeSet);
        return entries;
    }


}
