package com.example.home.logger;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;

import java.util.HashMap;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements GoogleMap.OnMarkerClickListener, OnMapReadyCallback, GoogleMap.OnCameraChangeListener, OnStreetViewPanoramaReadyCallback, StreetViewPanorama.OnStreetViewPanoramaCameraChangeListener {


    private GoogleMap mMap;
    private SupportStreetViewPanoramaFragment svpFragment;
    private StreetViewPanorama svp;
    float b_m, b_s;

    HashMap<Float, LatLng> results = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Intent intent = getIntent();
        results = (HashMap<Float, LatLng>) intent.getSerializableExtra("RESULTS");

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        svpFragment = ((SupportStreetViewPanoramaFragment)
                getSupportFragmentManager()
                        .findFragmentById(R.id.streetviewpanorama));
        svpFragment.getStreetViewPanoramaAsync(this);

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        for (Map.Entry<Float, LatLng> entries : results.entrySet()){
            float accuracy = entries.getKey();
            LatLng location = entries.getValue();

            mMap.addMarker(new MarkerOptions().position(location).title(String.valueOf(accuracy)));
            //mMap.moveCamera(CameraUpdateFactory.newLatLng(location));

            CameraPosition cp = new CameraPosition.Builder().target(location).tilt(b_s).zoom(16).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp),3000,null);
            mMap.setOnMarkerClickListener(this);

            mMap.setOnCameraChangeListener(this);
        }

    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        svp = streetViewPanorama;
//        StreetViewPanoramaLocation location = svp.getLocation();
//        svp.setPosition(location.links[0].panoId);

    }

    public boolean onMarkerClick(Marker m) {
        if(svp !=null) {
            svp.setPosition(m.getPosition());
            StreetViewPanoramaCamera camera =
                    new StreetViewPanoramaCamera.Builder(
                            svp.getPanoramaCamera()).bearing(b_m).build();

            svp.animateTo(camera, 2000);
            svp.setOnStreetViewPanoramaCameraChangeListener(this);
        }
        return false;
    }


    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

        b_m = cameraPosition.bearing;

    }
    public void onStreetViewPanoramaCameraChange(StreetViewPanoramaCamera svpCamera)
    {
        b_s = svpCamera.bearing;

    }

}
